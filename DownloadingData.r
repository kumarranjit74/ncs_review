library(XLConnect)
library("XML")
library(RCurl)
library(data.table)
#function to read data from URL and convert it to dataframe
create_start_data_frame <- function(url, year, state){
  urldata <- getURL(url)
  test_Data <- readHTMLTable(urldata)
  data1 <- as.data.frame(test_Data[2])
  
  no_of_rows = nrow(data1)*14  #12month + 1 total + 1%total 
  my_df <- data.frame(year= numeric(no_of_rows), month_name = factor( c("April", "May", "June", "July", 
                                                                        "August", "September","October", 
                                                                        "November","December", "January",
                                                                        "February","March", "Total", "%Total")),
                      district = numeric(no_of_rows), state = numeric(no_of_rows), 
                      counsellor_report = numeric(no_of_rows) )
  #assigning state name
  my_df$state = state
  
  for(district_count in  0: (nrow(data1)-1)){
    #assigning Year
    my_df$year[((district_count*14)+1):((district_count*14)+9)] = year
    my_df$year[((district_count*14)+10):((district_count*14)+14)] = year+1
    #assigning District name
    my_df$district[((district_count*14)+1):((district_count*14)+14)] = as.character(as.character(data1$NULL.V1)[district_count+1] ) 
    #assigning data
    test_row <- data1[district_count+1,]
    test_row <- test_row[-1]
    my_df$counsellor_report[((district_count*14)+1):((district_count*14)+14)] <- t(test_row) 
  }
  return(my_df)
}

all_india_df     <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/ALL%20INDIA/Counsellor%20Registration%20Distribution%20-%20State%20wise%20-%202015-16.html", 2015, "all_india")
AndmanAndNicobar <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/ANDAMAN%20AND%20NICOBAR%20ISLANDS/Counsellor%20Registration%20Distribution%20-%20Andaman%20And%20Nicobar%20Islands%20-%202015-16.html",2015,"Andman and Nicobar")
AndhraPradesh    <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/ANDHRA%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Andhra%20Pradesh%20-%202015-16.html", 2015, "Andhra Pradesh")
ArunachalPradesh <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/ARUNACHAL%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Arunachal%20Pradesh%20-%202015-16.html", 2015,"Arunachal Pradesh")
Assam            <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/ASSAM/Counsellor%20Registration%20Distribution%20-%20Assam%20-%202015-16.html",2015, "Assam")
Bihar            <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/BIHAR/Counsellor%20Registration%20Distribution%20-%20Bihar%20-%202015-16.html",2015,"Bihar")
Chandigarh       <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/CHANDIGARH/Counsellor%20Registration%20Distribution%20-%20Chandigarh%20-%202015-16.html",2015,"Chandigarh")
Chhatisgarh      <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/CHHATTISGARH/Counsellor%20Registration%20Distribution%20-%20Chhattisgarh%20-%202015-16.html",2015,"Chhatisgarh")
DadarAndNagar    <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/DADRA%20AND%20NAGAR%20HAVELI/Counsellor%20Registration%20Distribution%20-%20Dadra%20And%20Nagar%20Haveli%20-%202015-16.html",2015,"DadarAndNagar")
DamanAndDiu      <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/DAMAN%20AND%20DIU/Counsellor%20Registration%20Distribution%20-%20Daman%20And%20Diu%20-%202015-16.html",2015,"DamanAndDiu")
Goa              <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/GOA/Counsellor%20Registration%20Distribution%20-%20Goa%20-%202015-16.html",2015,"Goa")
Gujarat          <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/GUJARAT/Counsellor%20Registration%20Distribution%20-%20Gujarat%20-%202015-16.html",2015,"Gujarat")
Haryana          <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/HARYANA/Counsellor%20Registration%20Distribution%20-%20Haryana%20-%202015-16.html",2015,"Haryana")
HimachalPradesh  <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/HIMACHAL%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Himachal%20Pradesh%20-%202015-16.html",2015,"HimachalPradesh")
JammuAndKashmir  <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/JAMMU%20AND%20KASHMIR/Counsellor%20Registration%20Distribution%20-%20Jammu%20And%20Kashmir%20-%202015-16.html",2015,"JammuAndKashmir")
Jharkhand        <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/JHARKHAND/Counsellor%20Registration%20Distribution%20-%20Jharkhand%20-%202015-16.html",2015,"Jharkhand")
Karnataka        <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/KARNATAKA/Counsellor%20Registration%20Distribution%20-%20Karnataka%20-%202015-16.html",2015,"Karnataka")
Kerla            <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/KERALA/Counsellor%20Registration%20Distribution%20-%20Kerala%20-%202015-16.html",2015,"Kerla")
Lakshadeep       <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/LAKSHADWEEP/Counsellor%20Registration%20Distribution%20-%20Lakshadweep%20-%202015-16.html",2015,"Lakshadeep")
MadhyaPradesh    <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/MADHYA%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Madhya%20Pradesh%20-%202015-16.html",2015,"MadhyaPradesh")
Maharashtra      <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/MAHARASHTRA/Counsellor%20Registration%20Distribution%20-%20Maharashtra%20-%202015-16.html",2015,"Maharashtra")
Manipur          <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/MANIPUR/Counsellor%20Registration%20Distribution%20-%20Manipur%20-%202015-16.html",2015,"Manipur")
Meghalaya        <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/MEGHALAYA/Counsellor%20Registration%20Distribution%20-%20Meghalaya%20-%202015-16.html",2015,"Meghalaya")
Mizoram          <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/MIZORAM/Counsellor%20Registration%20Distribution%20-%20Mizoram%20-%202015-16.html",2015,"Mizoram")
Nagaland         <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/NAGALAND/Counsellor%20Registration%20Distribution%20-%20Nagaland%20-%202015-16.html",2015,"Nagaland")
NCTofDelhi       <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/NCT%20OF%20DELHI/Counsellor%20Registration%20Distribution%20-%20Nct%20Of%20Delhi%20-%202015-16.html",2015,"NCTofDelhi")
Odisha           <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/ODISHA/Counsellor%20Registration%20Distribution%20-%20Odisha%20-%202015-16.html",2015,"Odisha")
Puducherry       <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/PUDUCHERRY/Counsellor%20Registration%20Distribution%20-%20Puducherry%20-%202015-16.html",2015,"Puducherry")
Punjab           <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/PUNJAB/Counsellor%20Registration%20Distribution%20-%20Punjab%20-%202015-16.html",2015,"Punjab")
Rajsthan         <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/RAJASTHAN/Counsellor%20Registration%20Distribution%20-%20Rajasthan%20-%202015-16.html",2015,"Rajsthan")
Sikkim           <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/SIKKIM/Counsellor%20Registration%20Distribution%20-%20Sikkim%20-%202015-16.html",2015,"Sikkim")
TamilNadu        <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/TAMIL%20NADU/Counsellor%20Registration%20Distribution%20-%20Tamil%20Nadu%20-%202015-16.html",2015,"TamilNadu")
Telangana        <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/TELANGANA/Counsellor%20Registration%20Distribution%20-%20Telangana%20-%202015-16.html",2015,"Telangana")
Tripura          <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/TRIPURA/Counsellor%20Registration%20Distribution%20-%20Tripura%20-%202015-16.html",2015,"Tripura")
UttarPradesh     <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/UTTAR%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Uttar%20Pradesh%20-%202015-16.html",2015,"UttarPradesh")
Uttrakhand       <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/UTTARAKHAND/Counsellor%20Registration%20Distribution%20-%20Uttarakhand%20-%202015-16.html",2015,"Uttrakhand")
WestBengal       <- create_start_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/WEST%20BENGAL/Counsellor%20Registration%20Distribution%20-%20West%20Bengal%20-%202015-16.html",2015,"WestBengal")

Counselloer_report2015_16 <- rbind(all_india_df,AndmanAndNicobar,AndhraPradesh,ArunachalPradesh,Assam,Bihar,Chandigarh,Chhatisgarh,DadarAndNagar,
                                   DamanAndDiu,Goa,Gujarat,Haryana, HimachalPradesh,JammuAndKashmir,Jharkhand,Karnataka, Kerla,
                                   Lakshadeep,MadhyaPradesh,Maharashtra,Manipur,Meghalaya,Mizoram,Nagaland,NCTofDelhi,Odisha,
                                   Puducherry,Punjab,Rajsthan,Sikkim,TamilNadu,Telangana,Tripura,UttarPradesh,Uttrakhand, WestBengal)
write.csv(Counselloer_report2015_16,"/Users/ranjitkumar/Desktop/GitLab/Data ScienceProject/NationalCareerServiceProject/ncs_review/DataFrames/Counselloer_report2015_16.csv")

#function to read data from URL and convert it to dataframe
create_data_frame <- function(url, year, state){
  urldata <- getURL(url)
  test_Data <- readHTMLTable(urldata)
  data1 <- as.data.frame(test_Data[2])
  
  no_of_rows = nrow(data1)*14  #12month + 1 total + 1%total 
  #creating data frame
  #for(i in 1: nrow(data1)) {
  my_df <- data.frame(year= numeric(no_of_rows), month_name = factor( c("April", "May", "June", "July", 
                                                                        "August", "September","October", 
                                                                        "November","December", "January",
                                                                        "February","March", "Total", "%Total")),
                      district = numeric(no_of_rows), state = numeric(no_of_rows), 
                      counsellor_report = numeric(no_of_rows) )
  #assigning state name
  my_df$state = state
  
  for(district_count in  0: (nrow(data1)-1)){
    #assigning Year
    my_df$year[((district_count*14)+1):((district_count*14)+9)] = year
    my_df$year[((district_count*14)+10):((district_count*14)+14)] = year+1
    #assigning District name
    my_df$district[((district_count*14)+1):((district_count*14)+14)] = as.character(as.character(data1$NULL.V1)[district_count+1] ) 
    #assigning data
    test_row <- data1[district_count+1,]
    test_row <- test_row[-c(1,2,16)]
    my_df$counsellor_report[((district_count*14)+1):((district_count*14)+14)] <- t(test_row) 
  }
  return(my_df)
}

all_india_df<-  create_data_frame("https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/ALL%20INDIA/Counsellor%20Registration%20Distribution%20-%20State%20wise%20-%202016-2017.html", 2016, "all_india")
AndmanAndNicobar <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/ANDAMAN%20AND%20NICOBAR%20ISLANDS/Counsellor%20Registration%20Distribution%20-%20Andaman%20And%20Nicobar%20Islands%20-%202016-2017.html",2016,"Andman and Nicobar")
AndhraPradesh    <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/ANDHRA%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Andhra%20Pradesh%20-%202016-2017.html", 2016, "Andhra Pradesh")
ArunachalPradesh <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/ARUNACHAL%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Arunachal%20Pradesh%20-%202016-2017.html", 2016,"Arunachal Pradesh")
Assam            <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/ASSAM/Counsellor%20Registration%20Distribution%20-%20Assam%20-%202016-2017.html",2016, "Assam")
Bihar            <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/BIHAR/Counsellor%20Registration%20Distribution%20-%20Bihar%20-%202016-2017.html",2016,"Bihar")
Chandigarh       <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/CHANDIGARH/Counsellor%20Registration%20Distribution%20-%20Chandigarh%20-%202016-2017.html",2016,"Chandigarh")
Chhatisgarh      <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/CHHATTISGARH/Counsellor%20Registration%20Distribution%20-%20Chhattisgarh%20-%202016-2017.html",2016,"Chhatisgarh")
DadarAndNagar    <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/DADRA%20AND%20NAGAR%20HAVELI/Counsellor%20Registration%20Distribution%20-%20Dadra%20And%20Nagar%20Haveli%20-%202016-2017.html",2016,"DadarAndNagar")
DamanAndDiu      <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/DAMAN%20AND%20DIU/Counsellor%20Registration%20Distribution%20-%20Daman%20And%20Diu%20-%202016-2017.html",2016,"DamanAndDiu")
Goa              <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/GOA/Counsellor%20Registration%20Distribution%20-%20Goa%20-%202016-2017.html",2016,"Goa")
Gujarat          <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/GUJARAT/Counsellor%20Registration%20Distribution%20-%20Gujarat%20-%202016-2017.html",2016,"Gujarat")
Haryana          <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/HARYANA/Counsellor%20Registration%20Distribution%20-%20Haryana%20-%202016-2017.html",2016,"Haryana")
HimachalPradesh  <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/HIMACHAL%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Himachal%20Pradesh%20-%202016-2017.html",2016,"HimachalPradesh")
JammuAndKashmir  <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/JAMMU%20AND%20KASHMIR/Counsellor%20Registration%20Distribution%20-%20Jammu%20And%20Kashmir%20-%202016-2017.html",2016,"JammuAndKashmir")
Jharkhand        <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/JHARKHAND/Counsellor%20Registration%20Distribution%20-%20Jharkhand%20-%202016-2017.html",2016,"Jharkhand")
Karnataka        <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/KARNATAKA/Counsellor%20Registration%20Distribution%20-%20Karnataka%20-%202016-2017.html",2016,"Karnataka")
Kerla            <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/KERALA/Counsellor%20Registration%20Distribution%20-%20Kerala%20-%202016-2017.html",2016,"Kerla")
Lakshadeep       <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/LAKSHADWEEP/Counsellor%20Registration%20Distribution%20-%20Lakshadweep%20-%202016-2017.html",2016,"Lakshadeep")
MadhyaPradesh    <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/MADHYA%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Madhya%20Pradesh%20-%202016-2017.html",2016,"MadhyaPradesh")
Maharashtra      <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/MAHARASHTRA/Counsellor%20Registration%20Distribution%20-%20Maharashtra%20-%202016-2017.html",2016,"Maharashtra")
Manipur          <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/MANIPUR/Counsellor%20Registration%20Distribution%20-%20Manipur%20-%202016-2017.html",2016,"Manipur")
Meghalaya        <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/MEGHALAYA/Counsellor%20Registration%20Distribution%20-%20Meghalaya%20-%202016-2017.html",2016,"Meghalaya")
Mizoram          <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/MIZORAM/Counsellor%20Registration%20Distribution%20-%20Mizoram%20-%202016-2017.html",2016,"Mizoram")
Nagaland         <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/NAGALAND/Counsellor%20Registration%20Distribution%20-%20Nagaland%20-%202016-2017.html",2016,"Nagaland")
NCTofDelhi       <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/NCT%20OF%20DELHI/Counsellor%20Registration%20Distribution%20-%20Nct%20Of%20Delhi%20-%202016-2017.html",2016,"NCTofDelhi")
Odisha           <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/ODISHA/Counsellor%20Registration%20Distribution%20-%20Odisha%20-%202016-2017.html",2016,"Odisha")
Puducherry       <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/PUDUCHERRY/Counsellor%20Registration%20Distribution%20-%20Puducherry%20-%202016-2017.html",2016,"Puducherry")
Punjab           <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/PUNJAB/Counsellor%20Registration%20Distribution%20-%20Punjab%20-%202016-2017.html",2016,"Punjab")
Rajsthan         <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/RAJASTHAN/Counsellor%20Registration%20Distribution%20-%20Rajasthan%20-%202016-2017.html",2016,"Rajsthan")
Sikkim           <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/SIKKIM/Counsellor%20Registration%20Distribution%20-%20Sikkim%20-%202016-2017.html",2016,"Sikkim")
TamilNadu        <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/TAMIL%20NADU/Counsellor%20Registration%20Distribution%20-%20Tamil%20Nadu%20-%202016-2017.html",2016,"TamilNadu")
Telangana        <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/TELANGANA/Counsellor%20Registration%20Distribution%20-%20Telangana%20-%202016-2017.html",2016,"Telangana")
Tripura          <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/TRIPURA/Counsellor%20Registration%20Distribution%20-%20Tripura%20-%202016-2017.html",2016,"Tripura")
UttarPradesh     <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/UTTAR%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Uttar%20Pradesh%20-%202016-2017.html",2016,"UttarPradesh")
Uttrakhand       <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/UTTARAKHAND/Counsellor%20Registration%20Distribution%20-%20Uttarakhand%20-%202016-2017.html",2016,"Uttrakhand")
WestBengal       <- create_data_frame( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/WEST%20BENGAL/Counsellor%20Registration%20Distribution%20-%20West%20Bengal%20-%202016-2017.html",2016,"WestBengal")

Counselloer_report2016_17 <- rbind(all_india_df,AndmanAndNicobar,AndhraPradesh,ArunachalPradesh,Assam,Bihar,Chandigarh,Chhatisgarh,DadarAndNagar,
                                   DamanAndDiu,Goa,Gujarat,Haryana, HimachalPradesh,JammuAndKashmir,Jharkhand,Karnataka, Kerla,
                                   Lakshadeep,MadhyaPradesh,Maharashtra,Manipur,Meghalaya,Mizoram,Nagaland,NCTofDelhi,Odisha,
                                   Puducherry,Punjab,Rajsthan,Sikkim,TamilNadu,Telangana,Tripura,UttarPradesh,Uttrakhand, WestBengal)

write.csv(Counselloer_report2016_17,"/Users/ranjitkumar/Desktop/GitLab/Data ScienceProject/NationalCareerServiceProject/ncs_review/DataFrames/Counselloer_report2016_17.csv")


Data_unavailable_fill_NA <- function(no_of_rows, columnName){
  colOldName <- seq(1:no_of_rows)
  my_df <- data.frame(colOldName)
  my_df$colOldName <- NA
  colnames(my_df)[colnames(my_df)=="colOldName"] <- columnName
  return(my_df)
}

#function to read data from URL and convert it to dataframe
create_data_frame17 <- function(url, year, state){
  urldata <- getURL(url)
  test_Data <- readHTMLTable(urldata)
  data1 <- as.data.frame(test_Data[2])
  
  no_of_rows = nrow(data1)*14  #12month + 1 total + 1%total 
  #creating data frame
  my_df <- data.frame(year= numeric(no_of_rows), month_name = factor( c("April", "May", "June", "July", 
                                                                        "August", "September","October", 
                                                                        "November","December", "January",
                                                                        "February","March", "Total", "%Total")),
                      district = numeric(no_of_rows), state = numeric(no_of_rows), 
                      counsellor_report = numeric(no_of_rows) )
  #assigning state name
  my_df$state = state
  
  for(district_count in  0: (nrow(data1)-1)){
    #assigning Year
    my_df$year[((district_count*14)+1):((district_count*14)+9)] = year
    my_df$year[((district_count*14)+10):((district_count*14)+14)] = year+1
    #assigning District name
    my_df$district[((district_count*14)+1):((district_count*14)+14)] = as.character(as.character(data1$NULL.V1)[district_count+1] ) 
    #assigning data
    test_row <- data1[district_count+1,]
    test_row <- test_row[-c(1,2,3,17)]
    my_df$counsellor_report[((district_count*14)+1):((district_count*14)+14)] <- t(test_row) 
  }
  return(my_df)
}
all_india_df     <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/ALL%20INDIA/Counsellor%20Registration%20Distribution%20-%20State%20wise%20-%202017-18.html", 2017, "all_india")
AndmanAndNicobar <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/ANDAMAN%20AND%20NICOBAR%20ISLANDS/Counsellor%20Registration%20Distribution%20-%20Andaman%20And%20Nicobar%20Islands%20-%202017-18.html",2017,"Andman and Nicobar")
AndhraPradesh    <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/ANDHRA%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Andhra%20Pradesh%20-%202017-18.html", 2017, "Andhra Pradesh")
ArunachalPradesh <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/ARUNACHAL%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Arunachal%20Pradesh%20-%202017-18.html", 2017,"Arunachal Pradesh")
Assam            <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/ASSAM/Counsellor%20Registration%20Distribution%20-%20Assam%20-%202017-18.html",2017, "Assam")
Bihar            <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/BIHAR/Counsellor%20Registration%20Distribution%20-%20Bihar%20-%202017-18.html",2017,"Bihar")
Chandigarh       <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/CHANDIGARH/Counsellor%20Registration%20Distribution%20-%20Chandigarh%20-%202017-18.html",2017,"Chandigarh")
Chhatisgarh      <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/CHHATTISGARH/Counsellor%20Registration%20Distribution%20-%20Chhattisgarh%20-%202017-18.html",2017,"Chhatisgarh")
DadarAndNagar    <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/DADRA%20AND%20NAGAR%20HAVELI/Counsellor%20Registration%20Distribution%20-%20Dadra%20And%20Nagar%20Haveli%20-%202017-18.html",2017,"DadarAndNagar")
DamanAndDiu      <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/DAMAN%20AND%20DIU/Counsellor%20Registration%20Distribution%20-%20Daman%20And%20Diu%20-%202017-18.html",2017,"DamanAndDiu")
Goa              <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/GOA/Counsellor%20Registration%20Distribution%20-%20Goa%20-%202017-18.html",2017,"Goa")
Gujarat          <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/GUJARAT/Counsellor%20Registration%20Distribution%20-%20Gujarat%20-%202017-18.html",2017,"Gujarat")
Haryana          <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/HARYANA/Counsellor%20Registration%20Distribution%20-%20Haryana%20-%202017-18.html",2017,"Haryana")



FillWithNA <- function(DataFrame, year){
  
  numberOfRow = nrow(DataFrame)/14
  for(district_count in  0:(numberOfRow-1)) {
    #assigning Year
    DataFrame$year[((district_count*14)+1):((district_count*14)+9)] = year
    DataFrame$year[((district_count*14)+10):((district_count*14)+14)] = year+1
    DataFrame$counsellor_report[((district_count*14)+1):((district_count*14)+14)] <- NA
  }
  return(DataFrame)
}
##error in reading html, filling with zero
#HimachalPradesh  <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/HIMACHAL%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Himachal%20Pradesh%20-%202017-18.html",2017,"HimachalPradesh")
HimachalPradesh  <- FillWithNA(HimachalPradesh,2017)
##
JammuAndKashmir  <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/JAMMU%20AND%20KASHMIR/Counsellor%20Registration%20Distribution%20-%20Jammu%20And%20Kashmir%20-%202017-18.html",2017,"JammuAndKashmir")
Jharkhand        <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/JHARKHAND/Counsellor%20Registration%20Distribution%20-%20Jharkhand%20-%202017-18.html",2017,"Jharkhand")

##error in reading html, filling with zero
#Karnataka        <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/KARNATAKA/Counsellor%20Registration%20Distribution%20-%20Karnataka%20-%202017-18.html",2017,"Karnataka")
Karnataka        <- FillWithNA(Karnataka, 2017)

Kerla            <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/KERALA/Counsellor%20Registration%20Distribution%20-%20Kerala%20-%202017-18.html",2017,"Kerla")
Lakshadeep       <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/LAKSHADWEEP/Counsellor%20Registration%20Distribution%20-%20Lakshadweep%20-%202017-18.html",2017,"Lakshadeep")

##error in reading html, filling with zero
#MadhyaPradesh    <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/MADHYA%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Madhya%20Pradesh%20-%202017-18.html",2017,"MadhyaPradesh")
MadhyaPradesh   <- FillWithNA(MadhyaPradesh, 2017)
##
Maharashtra      <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/MAHARASHTRA/Counsellor%20Registration%20Distribution%20-%20Maharashtra%20-%202017-18.html",2017,"Maharashtra")

Manipur          <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/MANIPUR/Counsellor%20Registration%20Distribution%20-%20Manipur%20-%202017-18.html",2017,"Manipur")
Meghalaya        <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/MEGHALAYA/Counsellor%20Registration%20Distribution%20-%20Meghalaya%20-%202017-18.html",2017,"Meghalaya")
Mizoram          <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/MIZORAM/Counsellor%20Registration%20Distribution%20-%20Mizoram%20-%202017-18.html",2017,"Mizoram")
Nagaland         <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/NAGALAND/Counsellor%20Registration%20Distribution%20-%20Nagaland%20-%202017-18.html",2017,"Nagaland")
NCTofDelhi       <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/NCT%20OF%20DELHI/Counsellor%20Registration%20Distribution%20-%20Nct%20Of%20Delhi%20-%202017-18.html",2017,"NCTofDelhi")
Odisha           <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/ODISHA/Counsellor%20Registration%20Distribution%20-%20Odisha%20-%202017-18.html",2017,"Odisha")
Puducherry       <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/PUDUCHERRY/Counsellor%20Registration%20Distribution%20-%20Puducherry%20-%202017-18.html",2017,"Puducherry")
Punjab           <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/PUNJAB/Counsellor%20Registration%20Distribution%20-%20Punjab%20-%202017-18.html",2017,"Punjab")
Rajsthan         <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/RAJASTHAN/Counsellor%20Registration%20Distribution%20-%20Rajasthan%20-%202017-18.html",2017,"Rajsthan")
Sikkim           <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/SIKKIM/Counsellor%20Registration%20Distribution%20-%20Sikkim%20-%202017-18.html",2017,"Sikkim")
TamilNadu        <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/TAMIL%20NADU/Counsellor%20Registration%20Distribution%20-%20Tamil%20Nadu%20-%202017-18.html",2017,"TamilNadu")
Telangana        <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/TELANGANA/Counsellor%20Registration%20Distribution%20-%20Telangana%20-%202017-18.html",2017,"Telangana")
Tripura          <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/TRIPURA/Counsellor%20Registration%20Distribution%20-%20Tripura%20-%202017-18.html",2017,"Tripura")
UttarPradesh     <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/UTTAR%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Uttar%20Pradesh%20-%202017-18.html",2017,"UttarPradesh")
Uttrakhand       <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/UTTARAKHAND/Counsellor%20Registration%20Distribution%20-%20Uttarakhand%20-%202017-18.html",2017,"Uttrakhand")
WestBengal       <- create_data_frame17(  "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/WEST%20BENGAL/Counsellor%20Registration%20Distribution%20-%20West%20Bengal%20-%202017-18.html",2017,"WestBengal")

Counsellor_report2017_18 <- rbind(all_india_df,AndmanAndNicobar,AndhraPradesh,ArunachalPradesh,Assam,Bihar,Chandigarh,Chhatisgarh,DadarAndNagar,
                                  DamanAndDiu,Goa,Gujarat,Haryana, HimachalPradesh,JammuAndKashmir,Jharkhand,Karnataka, Kerla,
                                  Lakshadeep,MadhyaPradesh,Maharashtra,Manipur,Meghalaya,Mizoram,Nagaland,NCTofDelhi,Odisha,
                                  Puducherry,Punjab,Rajsthan,Sikkim,TamilNadu,Telangana,Tripura,UttarPradesh,Uttrakhand, WestBengal)


write.csv(Counsellor_report2017_18,"/Users/ranjitkumar/Desktop/GitLab/Data ScienceProject/NationalCareerServiceProject/ncs_review/DataFrames/Counsellor_report2017_18.csv")

#function to read data from URL and convert it to dataframe
create_data_frame18 <- function(url, year, state){
  urldata <- getURL(url)
  test_Data <- readHTMLTable(urldata)
  data1 <- as.data.frame(test_Data[2])
  
  no_of_rows = nrow(data1)*14  #12month + 1 total + 1%total 
  #creating data frame
  #for(i in 1: nrow(data1)) {
  my_df <- data.frame(year= numeric(no_of_rows), month_name = factor( c("April", "May", "June", "July", 
                                                                        "August", "September","October", 
                                                                        "November","December", "January",
                                                                        "February","March", "Total", "%Total")),
                      district = numeric(no_of_rows), state = numeric(no_of_rows), 
                      counsellor_report = numeric(no_of_rows) )
  #assigning state name
  my_df$state = state
  
  for(district_count in  0: (nrow(data1)-1)){
    #assigning Year
    my_df$year[((district_count*14)+1):((district_count*14)+9)] = year
    my_df$year[((district_count*14)+10):((district_count*14)+14)] = year+1
    #assigning District name
    my_df$district[((district_count*14)+1):((district_count*14)+14)] = as.character(as.character(data1$NULL.V1)[district_count+1] ) 
    #assigning data
    test_row <- data1[district_count+1,]
    test_row <- test_row[-c(1,2,3,4,18)]
    my_df$counsellor_report[((district_count*14)+1):((district_count*14)+14)] <- t(test_row) 
  }
  return(my_df)
}

all_india_df     <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/ALL%20INDIA/Counsellor%20Registration%20Distribution%20-%20State%20wise%20-%202018-19.html", 2018, "all_india")
AndmanAndNicobar <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/ANDAMAN%20AND%20NICOBAR%20ISLANDS/Counsellor%20Registration%20Distribution%20-%20Andaman%20And%20Nicobar%20Islands%20-%202018-19.html",2018,"Andman and Nicobar")
AndhraPradesh    <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/ANDHRA%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Andhra%20Pradesh%20-%202018-19.html", 2018, "Andhra Pradesh")
ArunachalPradesh <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/ARUNACHAL%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Arunachal%20Pradesh%20-%202018-19.html", 2018,"Arunachal Pradesh")
Assam            <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/ASSAM/Counsellor%20Registration%20Distribution%20-%20Assam%20-%202018-19.html",2018, "Assam")
Bihar            <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/BIHAR/Counsellor%20Registration%20Distribution%20-%20Bihar%20-%202018-19.html",2018,"Bihar")
Chandigarh       <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/CHANDIGARH/Counsellor%20Registration%20Distribution%20-%20Chandigarh%20-%202018-19.html",2018,"Chandigarh")
Chhatisgarh      <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/CHHATTISGARH/Counsellor%20Registration%20Distribution%20-%20Chhattisgarh%20-%202018-19.html",2018,"Chhatisgarh")
DadarAndNagar    <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/DADRA%20AND%20NAGAR%20HAVELI/Counsellor%20Registration%20Distribution%20-%20Dadra%20And%20Nagar%20Haveli%20-%202018-19.html",2018,"DadarAndNagar")
DamanAndDiu      <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/DAMAN%20AND%20DIU/Counsellor%20Registration%20Distribution%20-%20Daman%20And%20Diu%20-%202018-19.html",2018,"DamanAndDiu")
Goa              <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/GOA/Counsellor%20Registration%20Distribution%20-%20Goa%20-%202018-19.html",2018,"Goa")
Gujarat          <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/GUJARAT/Counsellor%20Registration%20Distribution%20-%20Gujarat%20-%202018-19.html",2018,"Gujarat")
Haryana          <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/HARYANA/Counsellor%20Registration%20Distribution%20-%20Haryana%20-%202018-19.html",2018,"Haryana")
HimachalPradesh  <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/HIMACHAL%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Himachal%20Pradesh%20-%202018-19.html",2018,"HimachalPradesh")
JammuAndKashmir  <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/JAMMU%20AND%20KASHMIR/Counsellor%20Registration%20Distribution%20-%20Jammu%20And%20Kashmir%20-%202018-19.html",2018,"JammuAndKashmir")
Jharkhand        <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/JHARKHAND/Counsellor%20Registration%20Distribution%20-%20Jharkhand%20-%202018-19.html",2018,"Jharkhand")
Karnataka        <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/KARNATAKA/Counsellor%20Registration%20Distribution%20-%20Karnataka%20-%202018-19.html",2018,"Karnataka")
Kerla            <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/KERALA/Counsellor%20Registration%20Distribution%20-%20Kerala%20-%202018-19.html",2018,"Kerla")
Lakshadeep       <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/LAKSHADWEEP/Counsellor%20Registration%20Distribution%20-%20Lakshadweep%20-%202018-19.html",2018,"Lakshadeep")
MadhyaPradesh    <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/MADHYA%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Madhya%20Pradesh%20-%202018-19.html",2018,"MadhyaPradesh")
Maharashtra      <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/MAHARASHTRA/Counsellor%20Registration%20Distribution%20-%20Maharashtra%20-%202018-19.html",2018,"Maharashtra")
Manipur          <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/MANIPUR/Counsellor%20Registration%20Distribution%20-%20Manipur%20-%202018-19.html",2018,"Manipur")
Meghalaya        <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/MEGHALAYA/Counsellor%20Registration%20Distribution%20-%20Meghalaya%20-%202018-19.html",2018,"Meghalaya")
Mizoram          <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/MIZORAM/Counsellor%20Registration%20Distribution%20-%20Mizoram%20-%202018-19.html",2018,"Mizoram")
Nagaland         <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/NAGALAND/Counsellor%20Registration%20Distribution%20-%20Nagaland%20-%202018-19.html",2018,"Nagaland")
NCTofDelhi       <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/NCT%20OF%20DELHI/Counsellor%20Registration%20Distribution%20-%20Nct%20Of%20Delhi%20-%202018-19.html",2018,"NCTofDelhi")
Odisha           <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/ODISHA/Counsellor%20Registration%20Distribution%20-%20Odisha%20-%202018-19.html",2018,"Odisha")
Puducherry       <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/PUDUCHERRY/Counsellor%20Registration%20Distribution%20-%20Puducherry%20-%202018-19.html",2018,"Puducherry")
Punjab           <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/PUNJAB/Counsellor%20Registration%20Distribution%20-%20Punjab%20-%202018-19.html",2018,"Punjab")
Rajsthan         <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/RAJASTHAN/Counsellor%20Registration%20Distribution%20-%20Rajasthan%20-%202018-19.html",2018,"Rajsthan")
Sikkim           <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/SIKKIM/Counsellor%20Registration%20Distribution%20-%20Sikkim%20-%202018-19.html",2018,"Sikkim")
TamilNadu        <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/TAMIL%20NADU/Counsellor%20Registration%20Distribution%20-%20Tamil%20Nadu%20-%202018-19.html",2018,"TamilNadu")
Telangana        <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/TELANGANA/Counsellor%20Registration%20Distribution%20-%20Telangana%20-%202018-19.html",2018,"Telangana")
Tripura          <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/TRIPURA/Counsellor%20Registration%20Distribution%20-%20Tripura%20-%202018-19.html",2018,"Tripura")
UttarPradesh     <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/UTTAR%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Uttar%20Pradesh%20-%202018-19.html",2018,"UttarPradesh")
Uttrakhand       <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/UTTARAKHAND/Counsellor%20Registration%20Distribution%20-%20Uttarakhand%20-%202018-19.html",2018,"TamilNadu")
WestBengal       <- create_data_frame18( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/WEST%20BENGAL/Counsellor%20Registration%20Distribution%20-%20West%20Bengal%20-%202018-19.html",2018,"WestBengal")
Counselloer_report2018_19 <- rbind(all_india_df,AndmanAndNicobar,AndhraPradesh,ArunachalPradesh,Assam,Bihar,Chandigarh,Chhatisgarh,DadarAndNagar,
                                   DamanAndDiu,Goa,Gujarat,Haryana, HimachalPradesh,JammuAndKashmir,Jharkhand,Karnataka, Kerla,
                                   Lakshadeep,MadhyaPradesh,Maharashtra,Manipur,Meghalaya,Mizoram,Nagaland,NCTofDelhi,Odisha,
                                   Puducherry,Punjab,Rajsthan,Sikkim,TamilNadu,Telangana,Tripura,UttarPradesh,Uttrakhand, WestBengal)
write.csv(Counselloer_report2018_19,"/Users/ranjitkumar/Desktop/GitLab/Data ScienceProject/NationalCareerServiceProject/ncs_review/DataFrames/Counselloer_report2018_19.csv")


#function to read data from URL and convert it to dataframe
create_data_frame19 <- function(url, year, state){
  urldata <- getURL(url)
  test_Data <- readHTMLTable(urldata)
  data1 <- as.data.frame(test_Data[2])
  
  no_of_rows = nrow(data1)*14  #12month + 1 total + 1%total 
  #creating data frame
  #for(i in 1: nrow(data1)) {
  my_df <- data.frame(year= numeric(no_of_rows), month_name = factor( c("April", "May", "June", "July", 
                                                                        "August", "September","October", 
                                                                        "November","December", "January",
                                                                        "February","March", "Total", "%Total")),
                      district = numeric(no_of_rows), state = numeric(no_of_rows), 
                      counsellor_report = numeric(no_of_rows) )
  #assigning state name
  my_df$state = state
  
  for(district_count in  0: (nrow(data1)-1)){
    #assigning Year
    my_df$year[((district_count*14)+1):((district_count*14)+9)] = year
    my_df$year[((district_count*14)+10):((district_count*14)+14)] = year+1
    #assigning District name
    my_df$district[((district_count*14)+1):((district_count*14)+14)] = as.character(as.character(data1$NULL.V1)[district_count+1] ) 
    #assigning data
    test_row <- data1[district_count+1,]
    test_row <- test_row[-c(1:5,19)]
    my_df$counsellor_report[((district_count*14)+1):((district_count*14)+14)] <- t(test_row) 
  }
  return(my_df)
}

all_india_df     <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/ALL%20INDIA/Counsellor%20Registration%20Distribution%20-%20State%20wise%20-%202019-20.html", 2019, "all_india")
AndmanAndNicobar <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/ANDAMAN%20AND%20NICOBAR%20ISLANDS/Counsellor%20Registration%20Distribution%20-%20Andaman%20And%20Nicobar%20Islands%20-%202019-20.html",2019,"Andman and Nicobar")
AndhraPradesh    <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/ANDHRA%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Andhra%20Pradesh%20-%202019-20.html", 2019, "Andhra Pradesh")
ArunachalPradesh <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/ARUNACHAL%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Arunachal%20Pradesh%20-%202019-20.html", 2019,"Arunachal Pradesh")
Assam            <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/ASSAM/Counsellor%20Registration%20Distribution%20-%20Assam%20-%202019-20.html",2019, "Assam")
Bihar            <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/BIHAR/Counsellor%20Registration%20Distribution%20-%20Bihar%20-%202019-20.html",2019,"Bihar")
Chandigarh       <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/CHANDIGARH/Counsellor%20Registration%20Distribution%20-%20Chandigarh%20-%202019-20.html",2019,"Chandigarh")
Chhatisgarh      <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/CHHATTISGARH/Counsellor%20Registration%20Distribution%20-%20Chhattisgarh%20-%202019-20.html",2019,"Chhatisgarh")
DadarAndNagar    <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/DADRA%20AND%20NAGAR%20HAVELI/Counsellor%20Registration%20Distribution%20-%20Dadra%20And%20Nagar%20Haveli%20-%202019-20.html",2019,"DadarAndNagar")
DamanAndDiu      <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/DAMAN%20AND%20DIU/Counsellor%20Registration%20Distribution%20-%20Daman%20And%20Diu%20-%202019-20.html",2019,"DamanAndDiu")
Goa              <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/GOA/Counsellor%20Registration%20Distribution%20-%20Goa%20-%202019-20.html",2019,"Goa")
Gujarat          <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/GUJARAT/Counsellor%20Registration%20Distribution%20-%20Gujarat%20-%202019-20.html",2019,"Gujarat")
Haryana          <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/HARYANA/Counsellor%20Registration%20Distribution%20-%20Haryana%20-%202019-20.html",2019,"Haryana")
HimachalPradesh  <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/HIMACHAL%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Himachal%20Pradesh%20-%202019-20.html",2019,"HimachalPradesh")
JammuAndKashmir  <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/JAMMU%20AND%20KASHMIR/Counsellor%20Registration%20Distribution%20-%20Jammu%20And%20Kashmir%20-%202019-20.html",2019,"JammuAndKashmir")
Jharkhand        <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/JHARKHAND/Counsellor%20Registration%20Distribution%20-%20Jharkhand%20-%202019-20.html",2019,"Jharkhand")
Karnataka        <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/KARNATAKA/Counsellor%20Registration%20Distribution%20-%20Karnataka%20-%202019-20.html",2019,"Karnataka")
Kerla            <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/KERALA/Counsellor%20Registration%20Distribution%20-%20Kerala%20-%202019-20.html",2019,"Kerla")
Lakshadeep       <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/LAKSHADWEEP/Counsellor%20Registration%20Distribution%20-%20Lakshadweep%20-%202019-20.html",2019,"Lakshadeep")
MadhyaPradesh    <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/MADHYA%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Madhya%20Pradesh%20-%202019-20.html",2019,"MadhyaPradesh")
Maharashtra      <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/MAHARASHTRA/Counsellor%20Registration%20Distribution%20-%20Maharashtra%20-%202019-20.html",2019,"Maharashtra")
Manipur          <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/MANIPUR/Counsellor%20Registration%20Distribution%20-%20Manipur%20-%202019-20.html",2019,"Manipur")
Meghalaya        <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/MEGHALAYA/Counsellor%20Registration%20Distribution%20-%20Meghalaya%20-%202019-20.html",2019,"Meghalaya")
Mizoram          <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/MIZORAM/Counsellor%20Registration%20Distribution%20-%20Mizoram%20-%202019-20.html",2019,"Mizoram")
Nagaland         <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/NAGALAND/Counsellor%20Registration%20Distribution%20-%20Nagaland%20-%202019-20.html",2019,"Nagaland")
NCTofDelhi       <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/DELHI/Counsellor%20Registration%20Distribution%20-%20Delhi%20-%202019-20.html",2019,"NCTofDelhi")
Odisha           <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/ODISHA/Counsellor%20Registration%20Distribution%20-%20Odisha%20-%202019-20.html",2019,"Odisha")
Puducherry       <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/PUDUCHERRY/Counsellor%20Registration%20Distribution%20-%20Puducherry%20-%202019-20.html",2019,"Puducherry")
Punjab           <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/PUNJAB/Counsellor%20Registration%20Distribution%20-%20Punjab%20-%202019-20.html",2019,"Punjab")
Rajsthan         <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/RAJASTHAN/Counsellor%20Registration%20Distribution%20-%20Rajasthan%20-%202019-20.html",2019,"Rajsthan")
Sikkim           <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/SIKKIM/Counsellor%20Registration%20Distribution%20-%20Sikkim%20-%202019-20.html",2019,"Sikkim")
TamilNadu        <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/TAMIL%20NADU/Counsellor%20Registration%20Distribution%20-%20Tamil%20Nadu%20-%202019-20.html",2019,"TamilNadu")
Telangana        <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/TELANGANA/Counsellor%20Registration%20Distribution%20-%20Telangana%20-%202019-20.html",2019,"Telangana")
Tripura          <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/TRIPURA/Counsellor%20Registration%20Distribution%20-%20Tripura%20-%202019-20.html",2019,"Tripura")
UttarPradesh     <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/UTTAR%20PRADESH/Counsellor%20Registration%20Distribution%20-%20Uttar%20Pradesh%20-%202019-20.html",2019,"UttarPradesh")
Uttrakhand       <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/UTTARAKHAND/Counsellor%20Registration%20Distribution%20-%20Uttarakhand%20-%202019-20.html",2019,"TamilNadu")
WestBengal       <- create_data_frame19( "https://www.ncs.gov.in/NCS%20Reports/Counsellor%20Reports/Counsellor%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/WEST%20BENGAL/Counsellor%20Registration%20Distribution%20-%20West%20Bengal%20-%202019-20.html",2019,"WestBengal")

Counselloer_report2019_20 <- rbind(all_india_df,AndmanAndNicobar,AndhraPradesh,ArunachalPradesh,Assam,Bihar,Chandigarh,Chhatisgarh,DadarAndNagar,
                                   DamanAndDiu,Goa,Gujarat,Haryana, HimachalPradesh,JammuAndKashmir,Jharkhand,Karnataka, Kerla,
                                   Lakshadeep,MadhyaPradesh,Maharashtra,Manipur,Meghalaya,Mizoram,Nagaland,NCTofDelhi,Odisha,
                                   Puducherry,Punjab,Rajsthan,Sikkim,TamilNadu,Telangana,Tripura,UttarPradesh,Uttrakhand, WestBengal)

write.csv(Counselloer_report2019_20,"/Users/ranjitkumar/Desktop/GitLab/Data ScienceProject/NationalCareerServiceProject/ncs_review/DataFrames/Counselloer_report2019_20.csv")
Counsellor_report <- rbind(Counselloer_report2015_16, Counselloer_report2016_17, Counsellor_report2017_18, Counselloer_report2018_19,Counselloer_report2019_20)
write.csv(Counsellor_report,"Counsellor_report.csv")


create_data_frameYear15 <- function(state, url){
  urldata <- getURL(url)
  test_Data <- readHTMLTable(urldata)
  data1 <- as.data.frame(test_Data[2])
  
  no_of_rows = nrow(data1)*14  #12month + 1 total + 1%total 
  my_df <- data.frame(year= numeric(no_of_rows), month_name = factor( c("April", "May", "June", "July", 
                                                                        "August", "September","October", 
                                                                        "November","December", "January",
                                                                        "February","March", "Total", "%Total")),
                      district = numeric(no_of_rows), state = numeric(no_of_rows), 
                      EmployersReg = numeric(no_of_rows) )
  
  my_df$state = state
  for(district_count in  0: (nrow(data1)-1)){
    #assigning Year
    my_df$year[((district_count*14)+1):((district_count*14)+9)] = 2015
    my_df$year[((district_count*14)+10):((district_count*14)+14)] = 2016
    #assigning District name
    my_df$district[((district_count*14)+1):((district_count*14)+14)] = as.character(as.character(data1$NULL.V1)[district_count+1] ) 
    #assigning data
    test_row <- data1[district_count+1,]
    test_row <- test_row[-1]
    my_df$EmployersReg[((district_count*14)+1):((district_count*14)+14)] <- t(test_row) 
  }
  
  return (my_df)
}


create_data_frameYear16 <- function(state, url){
  urldata <- getURL(url)
  test_Data <- readHTMLTable(urldata)
  data1 <- as.data.frame(test_Data[2])
  
  no_of_rows = nrow(data1)*14  #12month + 1 total + 1%total 
  my_df <- data.frame(year= numeric(no_of_rows), month_name = factor( c("April", "May", "June", "July", 
                                                                        "August", "September","October", 
                                                                        "November","December", "January",
                                                                        "February","March", "Total", "%Total")),
                      district = numeric(no_of_rows), state = numeric(no_of_rows), 
                      EmployersReg = numeric(no_of_rows) )
  
  my_df$state = state
  
  for(district_count in  0: (nrow(data1)-1)){
    #assigning Year
    my_df$year[((district_count*14)+1):((district_count*14)+9)] = 2016
    my_df$year[((district_count*14)+10):((district_count*14)+14)] = 2017
    #assigning District name
    my_df$district[((district_count*14)+1):((district_count*14)+14)] = as.character(as.character(data1$NULL.V1)[district_count+1] ) 
    #assigning data
    test_row <- data1[district_count+1,]
    test_row <- test_row[-c(1,2,16)]
    my_df$EmployersReg[((district_count*14)+1):((district_count*14)+14)] <- t(test_row) 
  }
  return (my_df)
}

create_data_frameYear17 <- function(state,url){
  urldata <- getURL(url)
  test_Data <- readHTMLTable(urldata)
  data1 <- as.data.frame(test_Data[2])
  
  no_of_rows = nrow(data1)*14  #12month + 1 total + 1%total 
  my_df <- data.frame(year= numeric(no_of_rows), month_name = factor( c("April", "May", "June", "July", 
                                                                        "August", "September","October", 
                                                                        "November","December", "January",
                                                                        "February","March", "Total", "%Total")),
                      district = numeric(no_of_rows), state = numeric(no_of_rows), 
                      EmployersReg = numeric(no_of_rows) )
  
  my_df$state = state
  for(district_count in  0: (nrow(data1)-1)){
    #assigning Year
    my_df$year[((district_count*14)+1):((district_count*14)+9)] = 2017
    my_df$year[((district_count*14)+10):((district_count*14)+14)] = 2018
    #assigning District name
    my_df$district[((district_count*14)+1):((district_count*14)+14)] = as.character(as.character(data1$NULL.V1)[district_count+1] ) 
    #assigning data
    test_row <- data1[district_count+1,]
    test_row <- test_row[-c(1,2,3,17)]
    my_df$EmployersReg[((district_count*14)+1):((district_count*14)+14)] <- t(test_row) 
  }  
  return (my_df)
}

Data_unavailable_fill_NA <- function(no_of_rows, state){
  my_df <- data.frame(year= numeric(no_of_rows), month_name = factor( c("April", "May", "June", "July", 
                                                                        "August", "September","October", 
                                                                        "November","December", "January",
                                                                        "February","March", "Total", "%Total")),
                      district = numeric(no_of_rows), state = numeric(no_of_rows), 
                      EmployersReg = numeric(no_of_rows) )
  
  my_df$state = state
  
  return(my_df)
}

create_data_frameYear18 <- function(state,url){
  urldata <- getURL(url)
  test_Data <- readHTMLTable(urldata)
  data1 <- as.data.frame(test_Data[2])
  
  no_of_rows = nrow(data1)*14  #12month + 1 total + 1%total 
  my_df <- data.frame(year= numeric(no_of_rows), month_name = factor( c("April", "May", "June", "July", 
                                                                        "August", "September","October", 
                                                                        "November","December", "January",
                                                                        "February","March", "Total", "%Total")),
                      district = numeric(no_of_rows), state = numeric(no_of_rows), 
                      EmployersReg = numeric(no_of_rows) )
  
  my_df$state = state
  
  for(district_count in  0: (nrow(data1)-1)){
    #assigning Year
    my_df$year[((district_count*14)+1):((district_count*14)+9)] = 2018
    my_df$year[((district_count*14)+10):((district_count*14)+14)] = 2019
    #assigning District name
    my_df$district[((district_count*14)+1):((district_count*14)+14)] = as.character(as.character(data1$NULL.V1)[district_count+1] ) 
    #assigning data
    test_row <- data1[district_count+1,]
    test_row <- test_row[-c(1,2,3,4,18)]
    my_df$EmployersReg[((district_count*14)+1):((district_count*14)+14)] <- t(test_row) 
  }
  return (my_df)
}


create_data_frameYear19 <- function(state,url){
  urldata <- getURL(url)
  test_Data <- readHTMLTable(urldata)
  data1 <- as.data.frame(test_Data[2])
  
  no_of_rows = nrow(data1)*14  #12month + 1 total + 1%total 
  my_df <- data.frame(year= numeric(no_of_rows), month_name = factor( c("April", "May", "June", "July", 
                                                                        "August", "September","October", 
                                                                        "November","December", "January",
                                                                        "February","March", "Total", "%Total")),
                      district = numeric(no_of_rows), state = numeric(no_of_rows), 
                      EmployersReg = numeric(no_of_rows) )
  
  my_df$state = state
  for(district_count in  0: (nrow(data1)-1)){
    #assigning Year
    my_df$year[((district_count*14)+1):((district_count*14)+9)] = 2019
    my_df$year[((district_count*14)+10):((district_count*14)+14)] = 2020
    #assigning District name
    my_df$district[((district_count*14)+1):((district_count*14)+14)] = as.character(as.character(data1$NULL.V1)[district_count+1] ) 
    #assigning data
    test_row <- data1[district_count+1,]
    test_row <- test_row[-c(1:5,19)]
    my_df$EmployersReg[((district_count*14)+1):((district_count*14)+14)] <- t(test_row) 
  } 
  return (my_df)
}

all_india_df     <- create_data_frameYear15("all_india" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/ALL%20INDIA/Employers%20Registration%20Distribution%20-%20State%20wise%20-%202015-16.html")
AndmanAndNicobar <- create_data_frameYear15("AndmanAndNicobar" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/ANDAMAN%20AND%20NICOBAR%20ISLANDS/Employers%20Registration%20Distribution%20-%20Andaman%20And%20Nicobar%20Islands%20-%202015-16.html")
AndhraPradesh    <- create_data_frameYear15("AndhraPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/ANDHRA%20PRADESH/Employers%20Registration%20Distribution%20-%20Andhra%20Pradesh%20-%202015-16.html")
ArunachalPradesh <- create_data_frameYear15("ArunachalPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/ARUNACHAL%20PRADESH/Employers%20Registration%20Distribution%20-%20Arunachal%20Pradesh%20-%202015-16.html")
Assam            <- create_data_frameYear15("Assam" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/ASSAM/Employers%20Registration%20Distribution%20-%20Assam%20-%202015-16.html")
Bihar            <- create_data_frameYear15("Bihar" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/BIHAR/Employers%20Registration%20Distribution%20-%20Bihar%20-%202015-16.html")
Chandigarh       <- create_data_frameYear15("Chandigarh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/CHANDIGARH/Employers%20Registration%20Distribution%20-%20Chandigarh%20-%202015-16.html")
Chhatisgarh      <- create_data_frameYear15("Chhatisgarh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/CHHATTISGARH/Employers%20Registration%20Distribution%20-%20Chhattisgarh%20-%202015-16.html")
DadarAndNagar    <- create_data_frameYear15("DadarAndNagar" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/DADRA%20AND%20NAGAR%20HAVELI/Employers%20Registration%20Distribution%20-%20Dadra%20And%20Nagar%20Haveli%20-%202015-16.html")
DamanAndDiu      <- create_data_frameYear15("DamanAndDiu" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/DAMAN%20AND%20DIU/Employers%20Registration%20Distribution%20-%20Daman%20And%20Diu%20-%202015-16.html")
Goa              <- create_data_frameYear15("Goa" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/GOA/Employers%20Registration%20Distribution%20-%20Goa%20-%202015-16.html")
Gujarat          <- create_data_frameYear15("Gujarat" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/GUJARAT/Employers%20Registration%20Distribution%20-%20Gujarat%20-%202015-16.html")
Haryana          <- create_data_frameYear15("Haryana" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/HARYANA/Employers%20Registration%20Distribution%20-%20Haryana%20-%202015-16.html")
HimachalPradesh  <- create_data_frameYear15("HimachalPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/HIMACHAL%20PRADESH/Employers%20Registration%20Distribution%20-%20Himachal%20Pradesh%20-%202015-16.html")
JammuAndKashmir  <- create_data_frameYear15("JammuAndKashmir" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/JAMMU%20AND%20KASHMIR/Employers%20Registration%20Distribution%20-%20Jammu%20And%20Kashmir%20-%202015-16.html")
Jharkhand        <- create_data_frameYear15("Jharkhand" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/JHARKHAND/Employers%20Registration%20Distribution%20-%20Jharkhand%20-%202015-16.html")
Karnataka        <- create_data_frameYear15("Karnataka" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/KARNATAKA/Employers%20Registration%20Distribution%20-%20Karnataka%20-%202015-16.html")
Kerla            <- create_data_frameYear15("Kerla" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/KERALA/Employers%20Registration%20Distribution%20-%20Kerala%20-%202015-16.html")
Lakshadeep       <- create_data_frameYear15("Lakshadeep" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/LAKSHADWEEP/Employers%20Registration%20Distribution%20-%20Lakshadweep%20-%202015-16.html")
MadhyaPradesh    <- create_data_frameYear15("MadhyaPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/MADHYA%20PRADESH/Employers%20Registration%20Distribution%20-%20Madhya%20Pradesh%20-%202015-16.html")
Maharashtra      <- create_data_frameYear15("Maharashtra" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/MAHARASHTRA/Employers%20Registration%20Distribution%20-%20Maharashtra%20-%202015-16.html")
Manipur          <- create_data_frameYear15("Manipur" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/MANIPUR/Employers%20Registration%20Distribution%20-%20Manipur%20-%202015-16.html")
Meghalaya        <- create_data_frameYear15("Meghalaya" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/MEGHALAYA/Employers%20Registration%20Distribution%20-%20Meghalaya%20-%202015-16.html")
Mizoram          <- create_data_frameYear15("Mizoram" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/MIZORAM/Employers%20Registration%20Distribution%20-%20Mizoram%20-%202015-16.html")
Nagaland         <- create_data_frameYear15("Nagaland" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/NAGALAND/Employers%20Registration%20Distribution%20-%20Nagaland%20-%202015-16.html")
NCTofDelhi       <- create_data_frameYear15("NCTofDelhi" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/NCT%20OF%20DELHI/Employers%20Registration%20Distribution%20-%20Nct%20Of%20Delhi%20-%202015-16.html")
Odisha           <- create_data_frameYear15("Odisha" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/ODISHA/Employers%20Registration%20Distribution%20-%20Odisha%20-%202015-16.html")
Puducherry       <- create_data_frameYear15("Puducherry" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/PUDUCHERRY/Employers%20Registration%20Distribution%20-%20Puducherry%20-%202015-16.html")
Punjab           <- create_data_frameYear15("Punjab" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/PUNJAB/Employers%20Registration%20Distribution%20-%20Punjab%20-%202015-16.html")
Rajsthan         <- create_data_frameYear15("Rajsthan" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/RAJASTHAN/Employers%20Registration%20Distribution%20-%20Rajasthan%20-%202015-16.html")
Sikkim           <- create_data_frameYear15("Sikkim" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/SIKKIM/Employers%20Registration%20Distribution%20-%20Sikkim%20-%202015-16.html")
TamilNadu        <- create_data_frameYear15("TamilNadu" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/TAMIL%20NADU/Employers%20Registration%20Distribution%20-%20Tamil%20Nadu%20-%202015-16.html")
Telangana        <- create_data_frameYear15("Telangana" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/TELANGANA/Employers%20Registration%20Distribution%20-%20Telangana%20-%202015-16.html")
Tripura          <- create_data_frameYear15("Tripura" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/TRIPURA/Employers%20Registration%20Distribution%20-%20Tripura%20-%202015-16.html")
UttarPradesh     <- create_data_frameYear15("UttarPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/UTTAR%20PRADESH/Employers%20Registration%20Distribution%20-%20Uttar%20Pradesh%20-%202015-16.html")
Uttrakhand       <- create_data_frameYear15("Uttrakhand" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/UTTARAKHAND/Employers%20Registration%20Distribution%20-%20Uttarakhand%20-%202015-16.html")
WestBengal       <- create_data_frameYear15("WestBengal" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2015-16%20(Pre%20Data%20Cleansing)/WEST%20BENGAL/Employers%20Registration%20Distribution%20-%20West%20Bengal%20-%202015-16.html")

EmpRegis_15 <- rbind(all_india_df,AndmanAndNicobar,AndhraPradesh,ArunachalPradesh,Assam,Bihar,Chandigarh,Chhatisgarh,DadarAndNagar,
                     DamanAndDiu,Goa,Gujarat,Haryana, HimachalPradesh,JammuAndKashmir,Jharkhand,Karnataka, Kerla,
                     Lakshadeep,MadhyaPradesh,Maharashtra,Manipur,Meghalaya,Mizoram,Nagaland,NCTofDelhi,Odisha,
                     Puducherry,Punjab,Rajsthan,Sikkim,TamilNadu,Telangana,Tripura,UttarPradesh,Uttrakhand, WestBengal)

write.csv(EmpRegis_15,"/Users/ranjitkumar/Desktop/GitLab/Data ScienceProject/NationalCareerServiceProject/ncs_review/DataFrames/EmpRegis_15.csv")

all_india_df     <- create_data_frameYear16("all_india" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/ALL%20INDIA/Employers%20Registration%20Distribution%20-%20State%20wise%20-%202016-2017.html")
AndmanAndNicobar <- create_data_frameYear16("AndmanAndNicobar" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/ANDAMAN%20AND%20NICOBAR%20ISLANDS/Employers%20Registration%20Distribution%20-%20Andaman%20And%20Nicobar%20Islands%20-%202016-2017%20.html")
AndhraPradesh    <- create_data_frameYear16("AndhraPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/ANDHRA%20PRADESH/Employers%20Registration%20Distribution%20-%20Andhra%20Pradesh%20-%202016-2017%20.html")
ArunachalPradesh <- create_data_frameYear16("ArunachalPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/ARUNACHAL%20PRADESH/Employers%20Registration%20Distribution%20-%20Arunachal%20Pradesh%20-%202016-2017%20.html")
Assam            <- create_data_frameYear16("Assam" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/ASSAM/Employers%20Registration%20Distribution%20-%20Assam%20-%202016-2017%20.html")
Bihar            <- create_data_frameYear16("Bihar" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/BIHAR/Employers%20Registration%20Distribution%20-%20Bihar%20-%202016-2017%20.html")
Chandigarh       <- create_data_frameYear16("Chandigarh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/CHANDIGARH/Employers%20Registration%20Distribution%20-%20Chandigarh%20-%202016-2017%20.html")
Chhatisgarh      <- create_data_frameYear16("Chhatisgarh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/CHHATTISGARH/Employers%20Registration%20Distribution%20-%20Chhattisgarh%20-%202016-2017%20.html")
DadarAndNagar    <- create_data_frameYear16("DadarAndNagar" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/DADRA%20AND%20NAGAR%20HAVELI/Employers%20Registration%20Distribution%20-%20Dadra%20And%20Nagar%20Haveli%20-%202016-2017%20.html")
DamanAndDiu      <- create_data_frameYear16("DamanAndDiu" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/DAMAN%20AND%20DIU/Employers%20Registration%20Distribution%20-%20Daman%20And%20Diu%20-%202016-2017%20.html")
Goa              <- create_data_frameYear16("Goa" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/GOA/Employers%20Registration%20Distribution%20-%20Goa%20-%202016-2017%20.html")
Gujarat          <- create_data_frameYear16("Gujarat" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/GUJARAT/Employers%20Registration%20Distribution%20-%20Gujarat%20-%202016-2017%20.html")
Haryana          <- create_data_frameYear16("Haryana" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/HARYANA/Employers%20Registration%20Distribution%20-%20Haryana%20-%202016-2017%20.html")
HimachalPradesh  <- create_data_frameYear16("HimachalPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/HIMACHAL%20PRADESH/Employers%20Registration%20Distribution%20-%20Himachal%20Pradesh%20-%202016-2017%20.html")
JammuAndKashmir  <- create_data_frameYear16("JammuAndKashmir" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/JAMMU%20AND%20KASHMIR/Employers%20Registration%20Distribution%20-%20Jammu%20And%20Kashmir%20-%202016-2017%20.html")
Jharkhand        <- create_data_frameYear16("Jharkhand" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/JHARKHAND/Employers%20Registration%20Distribution%20-%20Jharkhand%20-%202016-2017%20.html")
Karnataka        <- create_data_frameYear16("Karnataka" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/KARNATAKA/Employers%20Registration%20Distribution%20-%20Karnataka%20-%202016-2017%20.html")
Kerla            <- create_data_frameYear16("Kerla" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/KERALA/Employers%20Registration%20Distribution%20-%20Kerala%20-%202016-2017%20.html")
Lakshadeep       <- create_data_frameYear16("Lakshadeep" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/LAKSHADWEEP/Employers%20Registration%20Distribution%20-%20Lakshadweep%20-%202016-2017%20.html")
MadhyaPradesh    <- create_data_frameYear16("MadhyaPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/MADHYA%20PRADESH/Employers%20Registration%20Distribution%20-%20Madhya%20Pradesh%20-%202016-2017%20.html")
Maharashtra      <- create_data_frameYear16("Maharashtra" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/MAHARASHTRA/Employers%20Registration%20Distribution%20-%20Maharashtra%20-%202016-2017%20.html")
Manipur          <- create_data_frameYear16("Manipur" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/MANIPUR/Employers%20Registration%20Distribution%20-%20Manipur%20-%202016-2017%20.html")
Meghalaya        <- create_data_frameYear16("Meghalaya" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/MEGHALAYA/Employers%20Registration%20Distribution%20-%20Meghalaya%20-%202016-2017%20.html")
Mizoram          <- create_data_frameYear16("Mizoram" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/MIZORAM/Employers%20Registration%20Distribution%20-%20Mizoram%20-%202016-2017%20.html")
Nagaland         <- create_data_frameYear16("Nagaland" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/NAGALAND/Employers%20Registration%20Distribution%20-%20Nagaland%20-%202016-2017%20.html")
NCTofDelhi       <- create_data_frameYear16("NCTofDelhi" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/NCT%20OF%20DELHI/Employers%20Registration%20Distribution%20-%20Nct%20Of%20Delhi%20-%202016-2017%20.html")
Odisha           <- create_data_frameYear16("Odisha" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/ODISHA/Employers%20Registration%20Distribution%20-%20Odisha%20-%202016-2017%20.html")
Puducherry       <- create_data_frameYear16("Puducherry" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/PUDUCHERRY/Employers%20Registration%20Distribution%20-%20Puducherry%20-%202016-2017%20.html")
Punjab           <- create_data_frameYear16("Punjab" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/PUNJAB/Employers%20Registration%20Distribution%20-%20Punjab%20-%202016-2017%20.html")
Rajsthan         <- create_data_frameYear16("Rajsthan" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/RAJASTHAN/Employers%20Registration%20Distribution%20-%20Rajasthan%20-%202016-2017%20.html")
Sikkim           <- create_data_frameYear16("Sikkim" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/SIKKIM/Employers%20Registration%20Distribution%20-%20Sikkim%20-%202016-2017%20.html")
TamilNadu        <- create_data_frameYear16("TamilNadu" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/TAMIL%20NADU/Employers%20Registration%20Distribution%20-%20Tamil%20Nadu%20-%202016-2017%20.html")
Telangana        <- create_data_frameYear16("Telangana" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/TELANGANA/Employers%20Registration%20Distribution%20-%20Telangana%20-%202016-2017%20.html")
Tripura          <- create_data_frameYear16("Tripura" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/TRIPURA/Employers%20Registration%20Distribution%20-%20Tripura%20-%202016-2017%20.html")
UttarPradesh     <- create_data_frameYear16("UttarPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/UTTAR%20PRADESH/Employers%20Registration%20Distribution%20-%20Uttar%20Pradesh%20-%202016-2017%20.html")
Uttrakhand       <- create_data_frameYear16("Uttrakhand" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/UTTARAKHAND/Employers%20Registration%20Distribution%20-%20Uttarakhand%20-%202016-2017%20.html")
WestBengal       <- create_data_frameYear16("WestBengal" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2016-17%20(Pre%20Data%20Cleansing)/WEST%20BENGAL/Employers%20Registration%20Distribution%20-%20West%20Bengal%20-%202016-2017%20.html")

EmpRegis_16 <- rbind(all_india_df,AndmanAndNicobar,AndhraPradesh,ArunachalPradesh,Assam,Bihar,Chandigarh,Chhatisgarh,DadarAndNagar,
                     DamanAndDiu,Goa,Gujarat,Haryana, HimachalPradesh,JammuAndKashmir,Jharkhand,Karnataka, Kerla,
                     Lakshadeep,MadhyaPradesh,Maharashtra,Manipur,Meghalaya,Mizoram,Nagaland,NCTofDelhi,Odisha,
                     Puducherry,Punjab,Rajsthan,Sikkim,TamilNadu,Telangana,Tripura,UttarPradesh,Uttrakhand, WestBengal)

write.csv(EmpRegis_16,"/Users/ranjitkumar/Desktop/GitLab/Data ScienceProject/NationalCareerServiceProject/ncs_review/DataFrames/EmpRegis_16.csv")


#####error in reading URL, filling with NA
#all_india_df     <- create_data_frameYear17("all_india" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/ALL%20INDIA/Employers%20Registration%20Distribution%20-%20State%20wise%20-%202017-18.html")
#all_india_df     <- Data_unavailable_fill_NA(518,"all_india")
######
AndmanAndNicobar <- create_data_frameYear17("AndmanAndNicobar" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/ANDAMAN%20AND%20NICOBAR%20ISLANDS/Employers%20Registration%20Distribution%20-%20Andaman%20And%20Nicobar%20Islands%20-%202017-18.html")
AndhraPradesh    <- create_data_frameYear17("AndhraPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/ANDHRA%20PRADESH/Employers%20Registration%20Distribution%20-%20Andhra%20Pradesh%20-%202017-18.html")
ArunachalPradesh <- create_data_frameYear17("ArunachalPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/ARUNACHAL%20PRADESH/Employers%20Registration%20Distribution%20-%20Arunachal%20Pradesh%20-%202017-18.html")
Assam            <- create_data_frameYear17("Assam" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/ASSAM/Employers%20Registration%20Distribution%20-%20Assam%20-%202017-18.html")
Bihar            <- create_data_frameYear17("Bihar" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/BIHAR/Employers%20Registration%20Distribution%20-%20Bihar%20-%202017-18.html")
Chandigarh       <- create_data_frameYear17("Chandigarh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/CHANDIGARH/Employers%20Registration%20Distribution%20-%20Chandigarh%20-%202017-18.html")
Chhatisgarh      <- create_data_frameYear17("Chhatisgarh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/CHHATTISGARH/Employers%20Registration%20Distribution%20-%20Chhattisgarh%20-%202017-18.html")
DadarAndNagar    <- create_data_frameYear17("DadarAndNagar" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/DADRA%20AND%20NAGAR%20HAVELI/Employers%20Registration%20Distribution%20-%20Dadra%20And%20Nagar%20Haveli%20-%202017-18.html")
DamanAndDiu      <- create_data_frameYear17("DamanAndDiu" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/DAMAN%20AND%20DIU/Employers%20Registration%20Distribution%20-%20Daman%20And%20Diu%20-%202017-18.html")
Goa              <- create_data_frameYear17("Goa" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/GOA/Employers%20Registration%20Distribution%20-%20Goa%20-%202017-18.html")
Gujarat          <- create_data_frameYear17("Gujarat" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/GUJARAT/Employers%20Registration%20Distribution%20-%20Gujarat%20-%202017-18.html")
Haryana          <- create_data_frameYear17("Haryana" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/HARYANA/Employers%20Registration%20Distribution%20-%20Haryana%20-%202017-18.html")
HimachalPradesh  <- create_data_frameYear17("HimachalPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/HIMACHAL%20PRADESH/Employers%20Registration%20Distribution%20-%20Himachal%20Pradesh%20-%202017-18.html")
JammuAndKashmir  <- create_data_frameYear17("JammuAndKashmir" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/JAMMU%20AND%20KASHMIR/Employers%20Registration%20Distribution%20-%20Jammu%20And%20Kashmir%20-%202017-18.html")
Jharkhand        <- create_data_frameYear17("Jharkhand" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/JHARKHAND/Employers%20Registration%20Distribution%20-%20Jharkhand%20-%202017-18.html")
Karnataka        <- create_data_frameYear17("Karnataka" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/KARNATAKA/Employers%20Registration%20Distribution%20-%20Karnataka%20-%202017-18.html")
Kerla            <- create_data_frameYear17("Kerla" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/KERALA/Employers%20Registration%20Distribution%20-%20Kerala%20-%202017-18.html")
#####error in reading URL, filling with NA
#Lakshadeep       <- create_data_frameYear17("EmployersRegistration" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/LAKSHADWEEP/Employers%20Registration%20Distribution%20-%20Lakshadweep%20-%202017-18.html")
Lakshadeep     <- Data_unavailable_fill_NA(28,"Lakshadeep")
######
MadhyaPradesh    <- create_data_frameYear17("MadhyaPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/MADHYA%20PRADESH/Employers%20Registration%20Distribution%20-%20Madhya%20Pradesh%20-%202017-18.html")
Maharashtra      <- Data_unavailable_fill_NA(504,"Maharashtra")
Manipur          <- Data_unavailable_fill_NA(140,"Manipur")
Meghalaya        <- create_data_frameYear17("Meghalaya" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/MEGHALAYA/Employers%20Registration%20Distribution%20-%20Meghalaya%20-%202017-18.html")
Mizoram          <- create_data_frameYear17("Mizoram" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/MIZORAM/Employers%20Registration%20Distribution%20-%20Mizoram%20-%202017-18.html")
Nagaland         <- create_data_frameYear17("Nagaland" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/NAGALAND/Employers%20Registration%20Distribution%20-%20Nagaland%20-%202017-18.html")
##error in reading URL, filling with NA
#NCTofDelhi       <- create_data_frameYear17("EmployersRegistration" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/NCT%20OF%20DELHI/Employers%20Registration%20Distribution%20-%20Nct%20Of%20Delhi%20-%202017-18.html")
NCTofDelhi     <- Data_unavailable_fill_NA(140,"NCTofDelhi")
##
Odisha           <- create_data_frameYear17("Odisha" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/ODISHA/Employers%20Registration%20Distribution%20-%20Odisha%20-%202017-18.html")
Puducherry       <- create_data_frameYear17("Puducherry" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/PUDUCHERRY/Employers%20Registration%20Distribution%20-%20Puducherry%20-%202017-18.html")
Punjab           <- create_data_frameYear17("Punjab" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/PUNJAB/Employers%20Registration%20Distribution%20-%20Punjab%20-%202017-18.html")
Rajsthan         <- create_data_frameYear17("Rajsthan" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/RAJASTHAN/Employers%20Registration%20Distribution%20-%20Rajasthan%20-%202017-18.html")
Sikkim           <- create_data_frameYear17("Sikkim" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/SIKKIM/Employers%20Registration%20Distribution%20-%20Sikkim%20-%202017-18.html")
TamilNadu        <- create_data_frameYear17("TamilNadu" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/TAMIL%20NADU/Employers%20Registration%20Distribution%20-%20Tamil%20Nadu%20-%202017-18.html")
Telangana        <- create_data_frameYear17("Telangana" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/TELANGANA/Employers%20Registration%20Distribution%20-%20Telangana%20-%202017-18.html")
####error in reading URL, filling with NA
#Tripura          <- create_data_frameYear17("EmployersRegistration" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/TRIPURA/Employers%20Registration%20Distribution%20-%20Tripura%20-%202017-18.html")
Tripura     <- Data_unavailable_fill_NA(126,"Tripura")
###
UttarPradesh     <- create_data_frameYear17("UttarPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/UTTAR%20PRADESH/Employers%20Registration%20Distribution%20-%20Uttar%20Pradesh%20-%202017-18.html")
Uttrakhand       <- create_data_frameYear17("Uttrakhand" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/UTTARAKHAND/Employers%20Registration%20Distribution%20-%20Uttarakhand%20-%202017-18.html")
WestBengal       <- create_data_frameYear17("WestBengal" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2017-18%20(Pre%20Data%20Cleansing)/WEST%20BENGAL/Employers%20Registration%20Distribution%20-%20West%20Bengal%20-%202017-18.html")

EmpRegis_17 <- rbind(AndmanAndNicobar,AndhraPradesh,ArunachalPradesh,Assam,Bihar,Chandigarh,Chhatisgarh,DadarAndNagar,
                     DamanAndDiu,Goa,Gujarat,Haryana, HimachalPradesh,JammuAndKashmir,Jharkhand,Karnataka, Kerla,
                     Lakshadeep,MadhyaPradesh,Maharashtra,Manipur,Meghalaya,Mizoram,Nagaland,NCTofDelhi,Odisha,
                     Puducherry,Punjab,Rajsthan,Sikkim,TamilNadu,Telangana,Tripura,UttarPradesh,Uttrakhand, WestBengal)

write.csv(EmpRegis_17,"/Users/ranjitkumar/Desktop/GitLab/Data ScienceProject/NationalCareerServiceProject/ncs_review/DataFrames/EmpRegis_17.csv")

all_india_df     <- create_data_frameYear18("all_india" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/ALL%20INDIA/Employers%20Registration%20Distribution%20-%20State%20wise%20-%202018-19.html")
AndmanAndNicobar <- create_data_frameYear18("AndmanAndNicobar" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/ANDAMAN%20AND%20NICOBAR%20ISLANDS/Employers%20Registration%20Distribution%20-%20Andaman%20And%20Nicobar%20Islands%20-%202018-19.html")
AndhraPradesh    <- create_data_frameYear18("AndhraPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/ANDHRA%20PRADESH/Employers%20Registration%20Distribution%20-%20Andhra%20Pradesh%20-%202018-19.html")
ArunachalPradesh <- create_data_frameYear18("ArunachalPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/ARUNACHAL%20PRADESH/Employers%20Registration%20Distribution%20-%20Arunachal%20Pradesh%20-%202018-19.html")
Assam            <- create_data_frameYear18("Assam" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/ASSAM/Employers%20Registration%20Distribution%20-%20Assam%20-%202018-19.html")
Bihar            <- create_data_frameYear18("Bihar" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/BIHAR/Employers%20Registration%20Distribution%20-%20Bihar%20-%202018-19.html")
Chandigarh       <- create_data_frameYear18("Chandigarh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/CHANDIGARH/Employers%20Registration%20Distribution%20-%20Chandigarh%20-%202018-19.html")
Chhatisgarh      <- create_data_frameYear18("Chhatisgarh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/CHHATTISGARH/Employers%20Registration%20Distribution%20-%20Chhattisgarh%20-%202018-19.html")
DadarAndNagar    <- create_data_frameYear18("DadarAndNagar" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/DADRA%20AND%20NAGAR%20HAVELI/Employers%20Registration%20Distribution%20-%20Dadra%20And%20Nagar%20Haveli%20-%202018-19.html")
DamanAndDiu      <- create_data_frameYear18("DamanAndDiu" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/DAMAN%20AND%20DIU/Employers%20Registration%20Distribution%20-%20Daman%20And%20Diu%20-%202018-19.html")
Goa              <- create_data_frameYear18("Goa" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/GOA/Employers%20Registration%20Distribution%20-%20Goa%20-%202018-19.html")
Gujarat          <- create_data_frameYear18("Gujarat" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/GUJARAT/Employers%20Registration%20Distribution%20-%20Gujarat%20-%202018-19.html")
Haryana          <- create_data_frameYear18("Haryana" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/HARYANA/Employers%20Registration%20Distribution%20-%20Haryana%20-%202018-19.html")
HimachalPradesh  <- create_data_frameYear18("HimachalPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/HIMACHAL%20PRADESH/Employers%20Registration%20Distribution%20-%20Himachal%20Pradesh%20-%202018-19.html")
JammuAndKashmir  <- create_data_frameYear18("JammuAndKashmir" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/JAMMU%20AND%20KASHMIR/Employers%20Registration%20Distribution%20-%20Jammu%20And%20Kashmir%20-%202018-19.html")
Jharkhand        <- create_data_frameYear18("Jharkhand" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/JHARKHAND/Employers%20Registration%20Distribution%20-%20Jharkhand%20-%202018-19.html")
Karnataka        <- create_data_frameYear18("Karnataka" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/KARNATAKA/Employers%20Registration%20Distribution%20-%20Karnataka%20-%202018-19.html")
Kerla            <- create_data_frameYear18("Kerla" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/KERALA/Employers%20Registration%20Distribution%20-%20Kerala%20-%202018-19.html")
Lakshadeep       <- create_data_frameYear18("Lakshadeep" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/LAKSHADWEEP/Employers%20Registration%20Distribution%20-%20Lakshadweep%20-%202018-19.html")
MadhyaPradesh    <- create_data_frameYear18("MadhyaPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/MADHYA%20PRADESH/Employers%20Registration%20Distribution%20-%20Madhya%20Pradesh%20-%202018-19.html")
Maharashtra      <- create_data_frameYear18("Maharashtra" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/MAHARASHTRA/Employers%20Registration%20Distribution%20-%20Maharashtra%20-%202018-19.html")
Manipur          <- create_data_frameYear18("Manipur" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/MANIPUR/Employers%20Registration%20Distribution%20-%20Manipur%20-%202018-19.html")
Meghalaya        <- create_data_frameYear18("Meghalaya" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/MEGHALAYA/Employers%20Registration%20Distribution%20-%20Meghalaya%20-%202018-19.html")
Mizoram          <- create_data_frameYear18("Mizoram" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/MIZORAM/Employers%20Registration%20Distribution%20-%20Mizoram%20-%202018-19.html")
Nagaland         <- create_data_frameYear18("Nagaland" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/NAGALAND/Employers%20Registration%20Distribution%20-%20Nagaland%20-%202018-19.html")
NCTofDelhi       <- create_data_frameYear18("NCTofDelhi" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/NCT%20OF%20DELHI/Employers%20Registration%20Distribution%20-%20Nct%20Of%20Delhi%20-%202018-19.html")
Odisha           <- create_data_frameYear18("Odisha" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/ODISHA/Employers%20Registration%20Distribution%20-%20Odisha%20-%202018-19.html")
Puducherry       <- create_data_frameYear18("Puducherry" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/PUDUCHERRY/Employers%20Registration%20Distribution%20-%20Puducherry%20-%202018-19.html")
Punjab           <- create_data_frameYear18("Punjab" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/PUNJAB/Employers%20Registration%20Distribution%20-%20Punjab%20-%202018-19.html")
Rajsthan         <- create_data_frameYear18("Rajsthan" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/RAJASTHAN/Employers%20Registration%20Distribution%20-%20Rajasthan%20-%202018-19.html")
Sikkim           <- create_data_frameYear18("Sikkim" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/SIKKIM/Employers%20Registration%20Distribution%20-%20Sikkim%20-%202018-19.html")
TamilNadu        <- create_data_frameYear18("TamilNadu" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/TAMIL%20NADU/Employers%20Registration%20Distribution%20-%20Tamil%20Nadu%20-%202018-19.html")
Telangana        <- create_data_frameYear18("Telangana" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/TELANGANA/Employers%20Registration%20Distribution%20-%20Telangana%20-%202018-19.html")
Tripura          <- create_data_frameYear18("Tripura" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/TRIPURA/Employers%20Registration%20Distribution%20-%20Tripura%20-%202018-19.html")
UttarPradesh     <- create_data_frameYear18("UttarPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/UTTAR%20PRADESH/Employers%20Registration%20Distribution%20-%20Uttar%20Pradesh%20-%202018-19.html")
Uttrakhand       <- create_data_frameYear18("Uttrakhand" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/UTTARAKHAND/Employers%20Registration%20Distribution%20-%20Uttarakhand%20-%202018-19.html")
WestBengal       <- create_data_frameYear18("WestBengal" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2018-19%20(Pre%20Data%20Cleansing)/WEST%20BENGAL/Employers%20Registration%20Distribution%20-%20West%20Bengal%20-%202018-19.html")

EmpRegis_18 <- rbind(all_india_df,AndmanAndNicobar,AndhraPradesh,ArunachalPradesh,Assam,Bihar,Chandigarh,Chhatisgarh,DadarAndNagar,
                     DamanAndDiu,Goa,Gujarat,Haryana, HimachalPradesh,JammuAndKashmir,Jharkhand,Karnataka, Kerla,
                     Lakshadeep,MadhyaPradesh,Maharashtra,Manipur,Meghalaya,Mizoram,Nagaland,NCTofDelhi,Odisha,
                     Puducherry,Punjab,Rajsthan,Sikkim,TamilNadu,Telangana,Tripura,UttarPradesh,Uttrakhand, WestBengal)

write.csv(EmpRegis_18,"/Users/ranjitkumar/Desktop/GitLab/Data ScienceProject/NationalCareerServiceProject/ncs_review/DataFrames/EmpRegis_18.csv")


all_india_df     <- create_data_frameYear19("all_india" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/ALL%20INDIA/Employers%20Registration%20Distribution%20-%20State%20wise%20-%202019-20.html")
AndmanAndNicobar <- create_data_frameYear19("AndmanAndNicobar" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/ANDAMAN%20AND%20NICOBAR%20ISLANDS/Employers%20Registration%20Distribution%20-%20Andaman%20And%20Nicobar%20Islands%20-%202019-20.html")
AndhraPradesh    <- create_data_frameYear19("AndhraPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/ANDHRA%20PRADESH/Employers%20Registration%20Distribution%20-%20Andhra%20Pradesh%20-%202019-20.html")
ArunachalPradesh <- create_data_frameYear19("ArunachalPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/ARUNACHAL%20PRADESH/Employers%20Registration%20Distribution%20-%20Arunachal%20Pradesh%20-%202019-20.html")
Assam            <- create_data_frameYear19("Assam" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/ASSAM/Employers%20Registration%20Distribution%20-%20Assam%20-%202019-20.html")
Bihar            <- create_data_frameYear19("Bihar" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/BIHAR/Employers%20Registration%20Distribution%20-%20Bihar%20-%202019-20.html")
Chandigarh       <- create_data_frameYear19("Chandigarh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/CHANDIGARH/Employers%20Registration%20Distribution%20-%20Chandigarh%20-%202019-20.html")
Chhatisgarh      <- create_data_frameYear19("Chhatisgarh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/CHHATTISGARH/Employers%20Registration%20Distribution%20-%20Chhattisgarh%20-%202019-20.html")
DadarAndNagar    <- create_data_frameYear19("DadarAndNagar" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/DADRA%20AND%20NAGAR%20HAVELI/Employers%20Registration%20Distribution%20-%20Dadra%20And%20Nagar%20Haveli%20-%202019-20.html")
DamanAndDiu      <- create_data_frameYear19("DamanAndDiu" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/DAMAN%20AND%20DIU/Employers%20Registration%20Distribution%20-%20Daman%20And%20Diu%20-%202019-20.html")
Goa              <- create_data_frameYear19("Goa" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/GOA/Employers%20Registration%20Distribution%20-%20Goa%20-%202019-20.html")
Gujarat          <- create_data_frameYear19("Gujarat" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/GUJARAT/Employers%20Registration%20Distribution%20-%20Gujarat%20-%202019-20.html")
Haryana          <- create_data_frameYear19("Haryana" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/HARYANA/Employers%20Registration%20Distribution%20-%20Haryana%20-%202019-20.html")
HimachalPradesh  <- create_data_frameYear19("HimachalPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/HIMACHAL%20PRADESH/Employers%20Registration%20Distribution%20-%20Himachal%20Pradesh%20-%202019-20.html")
JammuAndKashmir  <- create_data_frameYear19("JammuAndKashmir" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/JAMMU%20AND%20KASHMIR/Employers%20Registration%20Distribution%20-%20Jammu%20And%20Kashmir%20-%202019-20.html")
Jharkhand        <- create_data_frameYear19("Jharkhand" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/JHARKHAND/Employers%20Registration%20Distribution%20-%20Jharkhand%20-%202019-20.html")
Karnataka        <- create_data_frameYear19("Karnataka" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/KARNATAKA/Employers%20Registration%20Distribution%20-%20Karnataka%20-%202019-20.html")
Kerla            <- create_data_frameYear19("Kerla" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/KERALA/Employers%20Registration%20Distribution%20-%20Kerala%20-%202019-20.html")
Lakshadeep       <- create_data_frameYear19("Lakshadeep" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/LAKSHADWEEP/Employers%20Registration%20Distribution%20-%20Lakshadweep%20-%202019-20.html")
MadhyaPradesh    <- create_data_frameYear19("MadhyaPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/MADHYA%20PRADESH/Employers%20Registration%20Distribution%20-%20Madhya%20Pradesh%20-%202019-20.html")
Maharashtra      <- create_data_frameYear19("Maharashtra" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/MAHARASHTRA/Employers%20Registration%20Distribution%20-%20Maharashtra%20-%202019-20.html")
Manipur          <- create_data_frameYear19("Manipur" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/MANIPUR/Employers%20Registration%20Distribution%20-%20Manipur%20-%202019-20.html")
Meghalaya        <- create_data_frameYear19("Meghalaya" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/MEGHALAYA/Employers%20Registration%20Distribution%20-%20Meghalaya%20-%202019-20.html")
Mizoram          <- create_data_frameYear19("Mizoram" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/MIZORAM/Employers%20Registration%20Distribution%20-%20Mizoram%20-%202019-20.html")
Nagaland         <- create_data_frameYear19("Nagaland" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/NAGALAND/Employers%20Registration%20Distribution%20-%20Nagaland%20-%202019-20.html")
NCTofDelhi       <- create_data_frameYear19("NCTofDelhi" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/DELHI/Employers%20Registration%20Distribution%20-%20Delhi%20-%202019-20.html")
Odisha           <- create_data_frameYear19("Odisha" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/ODISHA/Employers%20Registration%20Distribution%20-%20Odisha%20-%202019-20.html")
Puducherry       <- create_data_frameYear19("Puducherry" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/PUDUCHERRY/Employers%20Registration%20Distribution%20-%20Puducherry%20-%202019-20.html")
Punjab           <- create_data_frameYear19("Punjab" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/PUNJAB/Employers%20Registration%20Distribution%20-%20Punjab%20-%202019-20.html")
Rajsthan         <- create_data_frameYear19("Rajsthan" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/RAJASTHAN/Employers%20Registration%20Distribution%20-%20Rajasthan%20-%202019-20.html")
Sikkim           <- create_data_frameYear19("Sikkim" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/SIKKIM/Employers%20Registration%20Distribution%20-%20Sikkim%20-%202019-20.html")
TamilNadu        <- create_data_frameYear19("TamilNadu" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/TAMIL%20NADU/Employers%20Registration%20Distribution%20-%20Tamil%20Nadu%20-%202019-20.html")
Telangana        <- create_data_frameYear19("Telangana" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/TELANGANA/Employers%20Registration%20Distribution%20-%20Telangana%20-%202019-20.html")
Tripura          <- create_data_frameYear19("Tripura" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/TRIPURA/Employers%20Registration%20Distribution%20-%20Tripura%20-%202019-20.html")
UttarPradesh     <- create_data_frameYear19("UttarPradesh" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/UTTAR%20PRADESH/Employers%20Registration%20Distribution%20-%20Uttar%20Pradesh%20-%202019-20.html")
Uttrakhand       <- create_data_frameYear19("Uttrakhand" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/UTTARAKHAND/Employers%20Registration%20Distribution%20-%20Uttarakhand%20-%202019-20.html")
WestBengal       <- create_data_frameYear19("WestBengal" ,"https://www.ncs.gov.in/NCS%20Reports/Employer%20Reports/Employers%20Registration%20Distribution%20-%20State%20wise/2019-20(Post%20Data%20Cleansing)/WEST%20BENGAL/Employers%20Registration%20Distribution%20-%20West%20Bengal%20-%202019-20.html")

EmpRegis_19 <- rbind(all_india_df,AndmanAndNicobar,AndhraPradesh,ArunachalPradesh,Assam,Bihar,Chandigarh,Chhatisgarh,DadarAndNagar,
                     DamanAndDiu,Goa,Gujarat,Haryana, HimachalPradesh,JammuAndKashmir,Jharkhand,Karnataka, Kerla,
                     Lakshadeep,MadhyaPradesh,Maharashtra,Manipur,Meghalaya,Mizoram,Nagaland,NCTofDelhi,Odisha,
                     Puducherry,Punjab,Rajsthan,Sikkim,TamilNadu,Telangana,Tripura,UttarPradesh,Uttrakhand, WestBengal)

write.csv(EmpRegis_19,"/Users/ranjitkumar/Desktop/GitLab/Data ScienceProject/NationalCareerServiceProject/ncs_review/DataFrames/EmpRegis_19.csv")

Emp_reg <- rbind(EmpRegis_15, EmpRegis_16,EmpRegis_17,EmpRegis_18,EmpRegis_19)
write.csv(Emp_reg,"/Users/ranjitkumar/Desktop/GitLab/Data ScienceProject/NationalCareerServiceProject/ncs_review/DataFrames/Emp_reg.csv")



